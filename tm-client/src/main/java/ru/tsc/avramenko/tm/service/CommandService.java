package ru.tsc.avramenko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.repository.ICommandRepository;
import ru.tsc.avramenko.tm.api.service.ICommandService;
import ru.tsc.avramenko.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) return null;
        return commandRepository.getCommandByName(name);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByArg(@Nullable final String arg) {
        if (arg == null || arg.isEmpty()) return null;
        return commandRepository.getCommandByArg(arg);
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    public Collection<AbstractCommand> getArguments() {
        return commandRepository.getArguments();
    }

    @Override
    public Collection<String> getListCommandName() {
        return commandRepository.getCommandNames();
    }

    @Override
    public Collection<String> getListCommandArg() {
        return commandRepository.getCommandArg();
    }

    @Override
    public void add(@Nullable AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

}